package com.quotes.Dao;

import com.quotes.Model.Quote;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;

public interface QuoteRepository extends JpaRepository<Quote,Long>{
    Collection<Quote> findByContent(String content);
    Collection<Quote> findByAuthor(String author);
    Quote findQuoteById(Long id);
}
