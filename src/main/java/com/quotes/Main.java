package com.quotes;

import com.quotes.Dao.QuoteRepository;
import com.quotes.Model.Quote;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Arrays;

@SpringBootApplication
public class Main {
    public static void main(String[] args){
        SpringApplication.run(Main.class, args);
    }

    @Bean
    CommandLineRunner runner (QuoteRepository qr){
        return args -> {
            qr.save(new Quote("content","author"));
            qr.save(new Quote("c1","a1"));
            qr.save(new Quote("c2","a2"));

            qr.findAll().forEach(System.out::println);
            qr.findByAuthor("a1").forEach(System.out::println);
            qr.findByContent("c2").forEach(System.out::println);
        };
    }
}

