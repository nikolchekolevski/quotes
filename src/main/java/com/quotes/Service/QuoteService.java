package com.quotes.Service;

import com.quotes.Dao.QuoteRepository;
import com.quotes.Model.Quote;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class QuoteService {
    @Autowired
    private QuoteRepository qr;

    public QuoteService() {
    }

    public Collection<Quote> getAllQuotes(){
        return qr.findAll();
    }

    public Quote getQuoteById(Long id){
        return qr.findQuoteById(id);
    }

    public void deleteQuoteById(Long id){
        qr.delete(qr.findQuoteById(id));
    }

    public void updateQuote(Quote q){
        qr.save(q);
    }

    public void createQuote(Quote q){
        qr.save(q);
    }
}
