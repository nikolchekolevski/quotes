package com.quotes.Controller;


import com.quotes.Model.Quote;
import com.quotes.Service.QuoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/quotes")
public class QuoteController {

    @Autowired
    private QuoteService qs;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public Collection<Quote> getAllQuotes(){
        return qs.getAllQuotes();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Quote getQuoteById(@PathVariable("id") Long id){
        return qs.getQuoteById(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteQuoteById(@PathVariable("id") Long id){
        qs.deleteQuoteById(id);
    }

    @RequestMapping(method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void updateQuote(@RequestBody Quote q){
        qs.updateQuote(q);
    }

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void createQuote(@RequestBody Quote q){
        qs.createQuote(q);
    }
}
